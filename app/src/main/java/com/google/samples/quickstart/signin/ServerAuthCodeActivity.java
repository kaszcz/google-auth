package com.google.samples.quickstart.signin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.authcode.AuthManager;

/**
 * Demonstrates retrieving an offline access one-time code for the current Google user, which
 * can be exchanged by your server for an access token and refresh token.
 */
public class ServerAuthCodeActivity extends AppCompatActivity implements
        View.OnClickListener {

    public static final String TAG = "ServerAuthCodeActivity";
    private TextView mAuthCodeTextView;

    //LK001 - start
    AuthManager mAuthManager;
    //LK001 - end

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Views
        mAuthCodeTextView = (TextView) findViewById(R.id.detail);

        // Button click listeners
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.sign_out_button).setOnClickListener(this);
        findViewById(R.id.disconnect_button).setOnClickListener(this);

        //LK001 - start
        mAuthManager = AuthManager.getInstance(this);
        // For sample only: make sure there is a valid server client ID.
        mAuthManager.validateServerClientID();
        //LK001 - end
    }

    //get the Google AuthCode
    private void getAuthCode() {
        mAuthManager.getGoogleAuthCode(this, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // pass onActivityResults to AuthManager
        boolean isLoggedIn = mAuthManager.onGoogleAuthCodeResult(requestCode, resultCode, data);
        updateUI(isLoggedIn);
        if(isLoggedIn) {
            mAuthCodeTextView.setText(getString(R.string.auth_code_fmt, mAuthManager.getGoogleAuthCode()));
        }

    }

    private void signOut() {
        mAuthManager.signOut();
        // This should be done asynchronously on event coming from AuthManager.
        // Now we're just assuming that singOut process was successful
        updateUI(false);
    }

    private void revokeAccess() {
        mAuthManager.revokeAccess();
        // This should be done asynchronously on event coming from AuthManager.
        // Now we're just assuming that revokeAccess process was successful
        updateUI(false);
    }

    private void updateUI(boolean signedIn) {
        if (signedIn) {
            ((TextView) findViewById(R.id.status)).setText(R.string.signed_in);

            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.VISIBLE);
        } else {
            ((TextView) findViewById(R.id.status)).setText(R.string.signed_out);
            mAuthCodeTextView.setText(getString(R.string.auth_code_fmt, "null"));

            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                getAuthCode();
                break;
            case R.id.sign_out_button:
                signOut();
                break;
            case R.id.disconnect_button:
                revokeAccess();
                break;
        }
    }
}
