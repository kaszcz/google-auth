package com.authcode;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.samples.quickstart.signin.R;

public class AuthManager implements GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "AuthManager";

    private static AuthManager sInstance;
    private Context mContext;

    //Google
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_GET_AUTH_CODE = 9003;
    private String googleAuthCode;

    private AuthManager(Context context) {
        mContext = context;
    }

    public static AuthManager getInstance(Context context) {
        if (sInstance == null) {
            //Always pass in the Application Context
            sInstance = new AuthManager(context.getApplicationContext());
        }
        return sInstance;
    }

    public boolean validateServerClientID() {
        String serverGoogleClientId = mContext.getString(R.string.server_google_client_id);
        String suffix = ".apps.googleusercontent.com";
        if (!serverGoogleClientId.trim().endsWith(suffix)) {
            String message = mContext.getString(R.string.server_google_client_id) + suffix;
            Log.e(TAG, message);
            return false;
        } else {
            return true;
        }
    }

    //Google
    private void buildGoogleApiClient(android.support.v4.app.FragmentActivity fragmentActivity) {
        // [START configure_signin]
        // Configure sign-in to request offline access to the user's ID, basic
        // profile, and Google Drive. The first time you request a code you will
        // be able to exchange it for an access token and refresh token, which
        // you should store. In subsequent calls, the code will only result in
        // an access token. By asking for profile access (through
        // DEFAULT_SIGN_IN) you will also get an ID Token as a result of the
        // code exchange.
        String serverClientId = mContext.getString(R.string.server_google_client_id);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                .requestServerAuthCode(serverClientId)
                .requestEmail()
                .build();
        // [END configure_signin]

        // Build GoogleAPIClient with the Google Sign-In API and the above options.
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .enableAutoManage(fragmentActivity /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    public void getGoogleAuthCode(AppCompatActivity activity, android.support.v4.app.FragmentActivity fragmentActivity) {
        if(mGoogleApiClient == null) {
            buildGoogleApiClient(fragmentActivity);
        }

        // Start the retrieval process for a server auth code.  If requested, ask for a refresh
        // token.  Otherwise, only get an access token if a refresh token has been previously
        // retrieved.  Getting a new access token for an existing grant does not require
        // user consent.
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        activity.startActivityForResult(signInIntent, RC_GET_AUTH_CODE);
    }

    public boolean onGoogleAuthCodeResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_GET_AUTH_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(TAG, "onActivityResult:GET_AUTH_CODE:success:" + result.getStatus().isSuccess());

            if (result.isSuccess()) {
                // [START get_auth_code]
                GoogleSignInAccount acct = result.getSignInAccount();
                this.googleAuthCode = acct.getServerAuthCode();
                Log.i(TAG, "User is logged in to google with authCode=" + this.googleAuthCode);
                // Show signed-in UI.
                // TODO(user): send code to server and exchange for access/refresh/ID tokens.
                // [END get_auth_code]
                return true;
            } else {
                Log.i(TAG, "There was an error when logging in to Google, authCode=" + this.googleAuthCode);
                // Show signed-out UI.
                return false;
            }
        }
        return false;
    }

    public void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.i(TAG, "signOut:onResult:" + status);
                        // Show signed-out UI.
                        //TODO: send an event here
                    }
                });
    }

    public void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.i(TAG, "revokeAccess:onResult:" + status);
                        // Show signed-out UI.
                        //TODO: send an event here
                    }
                });
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.e(TAG, "onConnectionFailed:" + connectionResult);
    }

    public String getGoogleAuthCode() {
        return googleAuthCode;
    }
}
